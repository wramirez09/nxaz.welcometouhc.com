/**
 *   Application Logic
 */
'use strict';



// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};

//

// $.fn.flyout = function(options) {
//     var el = this;
//     var edge = options.edge || 80;
//     el.css({
//         position: "fixed",
//         top: "209px",
//         right: -1 * (el.width() - edge)
//     });
//     el.click(function() {
//         $(this).animate({
//             right: "0px"
//         });
//     });

//     el.mouseleave(function() {
//         $(this).animate({
//             right: -1 * (el.width() - edge)
//         });
//     });
// }

// $("#flyout").flyout({
//     "edge": "80"
// });


$(document).ready(function() {


    
    //tooltip 
    $(".whygroup").popover('show');

    // responsive video

    var video = $(".video-player");
    var windowObj = $(window);

    function onResizeWindow() {
        resizeVideo(video[0]);
    }

    function onLoadMetaData(e) {
        resizeVideo(e.target);
    }

    function resizeVideo(videoObject) {
        var percentWidth = videoObject.clientWidth * 100 / videoObject.videoWidth;
        var videoHeight = videoObject.videoHeight * percentWidth / 100;
        video.height(videoHeight);
    }

    video.on("loadedmetadata", onLoadMetaData);
    windowObj.resize(onResizeWindow);


    // ============================================== get json function ============================================= 


// var globalDataObject = $.getJSON("scripts/AZ-Plans.json");

    var MobilePlansList = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#PlansListMobileTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PlansListMobile').html(article_html);
        
        });
    }
    MobilePlansList();
    var getData = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#cont").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Plans').html(article_html);
            $(".planOption").hide("easeOut", function() {
                hideAndShowPlans();
            });

        });
    }
    getData();
    var ScriptTableData = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#PrescriptionData").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PrescriptionPlans').html(article_html);
        });
    }
    ScriptTableData();
    var dropdownList = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#DropdownTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#Dropdown').html(article_html);
        
        });
    }
    dropdownList();
    var PlanDetailTitle = function() {
        $.getJSON("scripts/AZ-Plans.json", function(data) {

            var templateSource = $("#PlanTitleTemplate").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#PlanTitle').html(article_html);

        });
    }
    PlanDetailTitle();

    Handlebars.registerHelper('if_eq', function(a, b, opts) {
        if(a == b) // Or === depending on your needs
            return opts.fn(this);
        else
            return opts.inverse(this);
    });

    var plantypelistData = function() {
        $.getJSON("scripts/az-data.json", function(data) {

            var templateSource = $("#plantypelist").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#MetalicLevelPlans').html(article_html);

        });
    }
    plantypelistData();


    var plantypelistFooter = function() {
        $.getJSON("scripts/az-data.json", function(data) {
            var templateSource = $("#plantypelistFooter").html()
            var template = Handlebars.compile(templateSource);
            var article_html = template(data);
            $('#FooterPlanSBCList').html(article_html);
        });
    }


    plantypelistFooter();



//home page pland filtering 

$(".FilterSection").on('click','#homePlanFamInd', function() {  
        $("#homePlanInd").prop('checked', false);

        //$("#homePlanFamInd").prop('checked', false);
        $("#Plans > li > div.OOPocket-Ind").fadeIn("easeOut");
        $("#Plans > li > div.Deduct-Fam").fadeIn("easeOut")
        $("#Plans > li > div.Deduct-Fam").fadeIn("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.fam").fadeIn("easeOut");
           $("#KeyBar > div.planKey > div > div.typerow > div.ind").animate({
                height:"70px"
            });
        $("#Plans > li > div.Deduct-Ind").animate({
            height:"70px"
        });

        $("#Plans > li > div.OOPocket-Fam").animate({
            height:"70px"
        });

         $(".plan-head").animate({
            height:"143px"
        });


        });

$(".FilterSection").on('click','#homePlanInd', function() {  
        $("#homePlanFamInd").prop('checked', false);
        $("#Plans > li > div.OOPocket-Ind").fadeOut("easeOut");
        $("#Plans > li > div.Deduct-Fam").fadeOut("easeOut")
        $("#Plans > li > div.Deduct-Fam").fadeOut("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.fam").fadeOut("easeOut");
        $("#KeyBar > div.planKey > div > div.typerow > div.ind").animate({
                height:"100px"
            });
        $("#Plans > li > div.Deduct-Ind").animate({
            height:"100px"
        });

        $("#Plans > li > div.OOPocket-Fam").animate({
            height:"100px"
        });

        $("#deduct-title").animate({
            height:"98px"
        });

        $(".plan-head").animate({
            height:"100px"
        });

        $("#plans").animte({
            height:"424px"
        });

        // $("#KeyBar > div.planKey > div:nth-child(3)").animate({
        //     height:"101px"
        // });



    });






 // ==================================== Sub Page Filtering ===================

    $("#PlanTitle").on('click','#IndRadio', function() {  
        $("#FamRadio").prop('checked', false);
        $(".fam-cell-icon").fadeOut('slow');
        $(".fam-cell-content").fadeOut('slow');
        $(".ind-cell-content").delay(700).queue(function(nxt) {

                $(this).removeClass('col-md-4').addClass('col-md-9');
                nxt();
        });
    });

    $("#PlanTitle").on('click','#FamRadio', function() {
        $("#IndRadio").prop('checked', false);
        $(".ind-cell-content").removeClass('col-md-9').addClass('col-md-4');                
        $(".fam-cell-icon").delay(1500).fadeIn('slow');
        $(".fam-cell-content").delay(1500).fadeIn('slow');
    });



    var currentSet = 1;
    var nxtstate;

    console.log("current level, dynamically set:" + currentSet);

    var increaseState = function() {
        currentSet++;
        console.log("increase set to" + currentSet);
    }


    var prescincreaseState = function() {
        prescCurrentLevel++;
        console.log("increase set to" + currentSet);
    }

    var decreaseState = function() {
        currentSet--;
        console.log("decreased set to:" + currentSet);
    }

    var prescdecreaseState = function() {
        prescCurrentLevel--;
        console.log("decreased set to:" + currentSet);
    }

    var setNewState = function(state) {
        var s = state;
        currentSet = state;
        var ns = currentSet;
        console.log("new state set to " + ns);
    }

    var prescsetNewState = function(state) {
        var s = state;
        prescCurrentLevel = state;
        var ns = currentSet;
        console.log("new state set to " + ns);
    }

    var setstatetolast = function() {
        currentSet = 8;
        console.log("set state to last is " + currentSet + "should be 8");
        // showPrevThree(currentSet);
    }

    var prescsetstatetolast = function() {
        prescCurrentLevel = 8;
        console.log("set state to last is " + currentSet + "should be 8");
        // showPrevThree(currentSet);
    }

    var setstatetoFirst = function(state) {
        var cl = state;
        if (cl == 8) {
            condole.log("this is 8:" + cl);
        } else {}
    }

   

    var hideAndShowPlans = function() {
        var po = $(".planOption");
        po.hide();
        $(".planOption").show("easeIn"); //start off by showing first three
    }

    var hideAllPlans = function() {
        $(".planOption").hide("easeOut");
    }

    var preschideAllPlans = function(){
    $("#PrescriptionPlans > li").hide();
}

    var showAllPlans = function() {
        $(".planOption").show("easeIn");
    }

     var precshowAllPlans = function() {
        $("#PrescriptionPlans > li").show("easeIn");
    }


// ============================= show next and previous 3 ================================



var setCurretArray = function(array){
    currentArray = array;
};


var getCurrentArray = function(){
    return currentArray;
};

   

 // plans table event handlers    
    
    var showNextThree = function(currentSet) {

            var curSet = currentSet;
            var easingFunction = "slow";
            var curSet = currentSet;
            console.log("the current set is" + curSet);


            if (curSet >= 8) {
                console.log("greater then 8");
                curSet = 0;
                console.log("reset, expect 0, set is:" + curSet);
                hideAllPlans()
                var startSlice = (curSet * 3);
                var endSlice = (startSlice + 3);
                console.log((startSlice) + " and " + (endSlice));
                $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
                $(".planOption").slice(startSlice, endSlice).show(easingFunction);
                curSet++;
                setNewState(curSet);

            }


            else if(curSet == 1){
                console.log("current set is 1" + "really its at" + curSet)
                
                var startSlice = (curSet * 3);
                var endSlice = (startSlice + 3);
                console.log((startSlice) + " and " + (endSlice));
                $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
                $("#Plans > li:nth-child(4)").css("margin-left","185px");
                hideAllPlans();
                $(".planOption").slice(startSlice, endSlice).show(easingFunction);
                curSet++;
                setNewState(curSet);
            } 


            else {
                var curSet = currentSet;
                var startSlice = (curSet * 3);
                var endSlice = (startSlice + 3);
                console.log((startSlice) + " and " + (endSlice));
                $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
                hideAllPlans();
                $(".planOption").slice(startSlice, endSlice).show(easingFunction);
                curSet++;
                setNewState(curSet);

            }
    }



var showPrevThree = function(currentSet) {
        var easingFunction = "slow";
        var newstate = currentSet;
        if (newstate == 1) {
            newstate = 8;
            setNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            hideAllPlans()
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);


        } else if (newstate > 1) {
            console.log(newstate);
            newstate--;
            console.log(newstate);
            setNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".planOption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "185px");
            hideAllPlans()
            $(".planOption").slice(startSlice, endSlice).show(easingFunction);

        }
      
    }


// presc table event handlers / carousel btn

var prescCurrentLevel = 1;
console.log("presc current level, dynamically set:" + prescCurrentLevel);

  var prescShowNextThree = function(currentSet) {


            var easingFunction = "slow";

            var curSet = currentSet;
            console.log("the current set is" + curSet);
            if (curSet >= 8) {
                console.log("greater then 8");
                curSet = 0;
                console.log("reset, expect 0, set is:" + curSet);
                var startSlice = (curSet * 3);
                var endSlice = (startSlice + 3);
                console.log((startSlice) + " and " + (endSlice));
                $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
                preschideAllPlans();
                $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

                curSet++;
                prescsetNewState(curSet);

            } else {
                var curSet = currentSet;
         
                var startSlice = (curSet * 3);
                var endSlice = (startSlice + 3);
                console.log((startSlice) + " and " + (endSlice));
                $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
                preschideAllPlans();
                $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

                prescincreaseState();

            }
    }


    var prescShowPrevThree = function(currentSet) {
        var easingFunction = "slow";
        var newstate = currentSet;
        if (newstate == 1) {
            newstate = 8;
            prescsetNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans()
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);


        } else if (newstate > 1) {
            console.log(newstate);
            newstate--;
            console.log(newstate);
            prescsetNewState(newstate);
            var endSlice = (newstate * 3);
            var startSlice = (endSlice - 3);
            console.log((startSlice) + " and " + (endSlice));
            $(".prescPlanoption:nth-child(" + (startSlice + 1) + ")").css("margin-left", "159px");
            preschideAllPlans()
            $(".prescPlanoption").slice(startSlice, endSlice).show(easingFunction);

        }
      
    }





// ======================================== carousel btns ========================================

    // ============================== PLANS

    var nextBtn = $("#arrow-right");
    var prevBtn = $("#arrow-left");

    nextBtn.on("click", function() {

        showNextThree(currentSet);
    });

    prevBtn.on("click", function() {
        showPrevThree(currentSet);
    });



    // =================================== PRESC

    

    var precnextBtn = $("#presc-arrow-right");
    var precprevBtn = $("#presc-arrow-left");

    precnextBtn.on("click", function() {
        console.log("prec  next hit ");
        prescShowNextThree(prescCurrentLevel);
    });

    precprevBtn.on("click", function() {
        console.log("prec prev hit ");
        prescShowPrevThree(prescCurrentLevel);
    });




    // these functions are used  to toggle off other check&radio btns when on is clicked
    $("input[type='checkbox']").on("change", function() {
        if (this.checked) {
            $(this).closest('.FilterSection')
                .find('input[type=checkbox]').not(this)
                .prop('checked', false);
        }

        else{}
    });

    $("input[type='radio']").on("change", function() {
        if (this.checked) {
            $(this).closest('.FilterSection')
                .find('input[type=radio]').not(this)
                .prop('checked', false);
        }

        else{}
    });


    // ========================== smooth scrolling function ====================

    var $root = $('html, body');
    $('.mainnav > div > ul > li > a').click(function() {
        _satellite.track(window.location.hash);

        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, "easeOut", function() {
            window.location.hash = href;
        });
        return false;
    });

    $('#FatFooter > div.iconrow.hidden-xs > div > div > div > div > a').click(function() {
        _satellite.track(window.location.hash);
        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, "easeOut", function() {
            window.location.hash = href;
        });
        return false;
    });



    // ===================================== autocomplete data array ============================ 
    $(function() {

        var availableTags = [
             { value: "NYC", url: 'http://www.nyc.com' }, 
             { value: "LA", url: 'http://www.la.com' },
             { value: "Philly", url: 'http://www.philly.com' },
             { value: "Chitown", url: 'http://www.chitown.com' },
             { value: "DC", url: 'http://www.washingtondc.com' },
             { value: "SF", url: 'http://www.sanfran.com' },
             { value: "Peru", url: 'http://www.peru.com' }
        ];
        // invoke jquery ui for autocomplete 

        $("#tags").autocomplete({
        source: availableTags,
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            go(ui.item.url);
        },
        
        focus: function (event, ui) {
            currentUrl = ui.item.url;
        }
    });
});

function go(url) {
    window.open(url);
}

function btnGoClick() {
    if (currentUrl !== "") go(currentUrl);
}



    // caret button that trigger/enables the rendering of the autocomplete button
    var button = $("#dropDownToggle");
    button.click(function(event) {
        var input = $("#tags");
        input.focus();
        input.autocomplete("search", "");
        console.log("caret clicked");
    });


    // =================================== video reveal function ==============================

    // responsive video

    var video = $(".video-player");
    var windowObj = $(window);

    function onResizeWindow() {
        resizeVideo(video[0]);
    }

    function onLoadMetaData(e) {
        resizeVideo(e.target);
    }

    function resizeVideo(videoObject) {
        var percentWidth = videoObject.clientWidth * 100 / videoObject.videoWidth;
        var videoHeight = videoObject.videoHeight * percentWidth / 100;
        video.height(videoHeight);
    }

    video.on("loadedmetadata", onLoadMetaData);
    windowObj.resize(onResizeWindow);


    var ToggleText = function(sm,sl){
        console.log("object one " + sl + " object two " + sm);

        var smEl = sm; 
        var slEl = sl;

        slEl.toggleClass("hidden");
        sEl.toggleClass("hidden");
        
    } 

    function videoRevel(IdOfVideoDiv, IdofBtn, clickedclass, smtID, slID) {
        var clickedclass = clickedclass;
        var video = IdOfVideoDiv;
        var btn = IdofBtn;
        var classToggle =  clickedclass;
        var smt = smtID;
        var slt = slID;
        btn.click(function(smt,slt) {
            video.slideToggle("easeOut");
            btn.toggleClass(classToggle);
            
            ToggleText(smtID, slID);
   
        });


    }

    // ============================== needs to be refactored DRY ===============================


    //section 1 video and btn
    var sec1video = $('#video-player-sec1');
    var sec2btn = $('#sec1btn-vid');
    sec1video.fadeOut("fast");
    sec1video.on("Click", videoRevel(sec1video, sec2btn, "fa-youtube-play"));

    //section 3 arizona details see more 
    var sec3btn = $("#arz-seemore-btn");    
    var sec3div = $("#arz-plan-details");
    var sec3seemore = $("#sec3seeMore");
    var sec3seeless = $("#sec3seeLess");
    sec3div.fadeOut("fast");
    sec3btn.on("Click", videoRevel(sec3div, sec3btn, "fa-minus-square", sec3seemore, sec3seeless));

    //section 3 video reveal

    var sec3VidBtn = $("#sec3-vid-btn");
    var sec3VidDiv = $("#video-player-sec3");
    sec3VidDiv.fadeOut("fast");
    sec3VidBtn.on("Click", videoRevel(sec3VidDiv, sec3VidBtn, "fa-youtube-play"));

    //sec 4 more details reveal
    var sec4BTn = $("#sec4seeMoreBTn");
    var sec4DivCont = $(".sec4-hidden-cont");
    var sec4seemore = $("#sec4seeMore");
    var sec4seeless = $("#sec4seeLess");
    sec4DivCont.fadeOut("fast");
    sec4BTn.on("Click", videoRevel(sec4DivCont, sec4BTn, "fa-minus-square", sec4seemore, sec4seeless));


    // section 4 video reveal
    var sec4VidBtn = $('#sec4-vid-btn');
    var sec4VidCont = $('#sec4-video');
    sec4VidCont.fadeOut("fast");
    sec4VidBtn.on("Click", videoRevel(sec4VidCont, sec4VidBtn, "fa-youtube-play"));

    // section 5 cont reveal
    var sec5contBtn = $('#sec5seeMoreBtn');
    var sec5DivCont = $('#sec5HiddenContent');
    sec5DivCont.fadeOut("fast");
    sec5contBtn.on("Click", videoRevel(sec5DivCont, sec5contBtn, "fa-minus-square"));

    //sec 5 video reveal
    var sec5vidBtn = $('#sec5-vid-btn');
    var sec5DivVid = $('#video-player-sec5');
    sec5DivVid.fadeOut("fast");
    sec5vidBtn.on("Click", videoRevel(sec5DivVid, sec5vidBtn, "fa-youtube-play"));


    //section 6 vid & btn
    var sec6btn = $('#sec6-vid-btn');
    var sec6Div = $('#sec6-video');
    sec6Div.fadeOut("fast");
    sec6btn.on("Click", videoRevel(sec6Div, sec6btn, "fa-youtube-play"));

    /* ================================    more details button   ===========================*/

    // used for icon switchin from postaive icon button to negative.

    function switchIcon(el, sClass) {
        var switchClass = sClass;
        var btn = el;
        btn.on("click", function() {
            $(this).toggleClass(switchClass)
        });

    };


    var moreDetailsBtn = $('.moreDetailsBtn');
    moreDetailsBtn.on("Click", switchIcon(moreDetailsBtn, "fa-minus-square"));



    // =================================== footer reveal function =============================
    $("#FatFooter").hide();

    $(".dropsprite").click(function() {

        $("#FatFooter").slideToggle();
        $('html, body').animate({
            scrollTop: $(document).height()
        }, 'slow');
        $(this).toggleClass("dropsprite-down");
        $(this).toggleClass("dropsprite-up");
    });


    $('body').scrollspy({
        target: '.navbar-collapse'
    });







}); //end of doc.ready function
