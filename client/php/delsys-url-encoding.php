<!-- Here's the snipped I think you're looking for - the $cobrand['SEARCH_XML'] bit can probably be ignored  -->
<?php
// base values
$url = 'https://www.geoaccess.com/uhc/po/GatewayXML.asp?xmlblock=';
$xml_encoded = '%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22%3F%3E%0D%0A%3C%21DOCTYPE+UNITED+SYSTEM+%22https%3A%2F%2Fwww.geoaccess.com%2Fuhc%2Fpo%2Fdtd%2FUHC.dtd%22%3E%0D%0A';
		
// build the xml string
ob_start(); 

<UNITED><PortalRequest><Options displayProviderId='Y' /><InitialSearchType type="<?=$InitialSearchType?>"/><ProductList><Product delsys='<?=$delsys?>' /></ProductList></PortalRequest></UNITED>
		
//// encode it and send the redirect header
$xml .= ob_get_clean();
$xml = str_replace("\n","",$xml);
		
if ($cobrand['SEARCH_XML']) 
{
	$xml_encoded = urlencode($cobrand['SEARCH_XML']);
} else { 
	$xml_encoded .= urlencode($xml);
}
header("Location: " . $url . $xml_encoded);
?>